using Stipple
using StippleUI

using Dates

using DataFrames
using Arrow
using Tables
using Distances
using PythonCall

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const EXTERNAL_DATA_PATH = joinpath("data", "external")

using PythonCall
fasttext = pyimport("fasttext")
ft = fasttext.load_model(joinpath(EXTERNAL_DATA_PATH, "cc.nl.64.bin"))

const upl = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl.arrow")))
const sdg = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sdg.arrow")))
const grondslagen = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "grondslagen.arrow")))
const upl2sdg = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl2sdg.arrow")))
const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))
const upl2sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl2sc.arrow")))
const sc_sentences = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc_sentences.arrow")))

const senmat = [sc_sentences.Sentence_Vec...;;]
const rdf = sc_sentences[:, [:recordPosition]]

const dist = CosineDist()

mutable struct LifeEvent
    date::Date
    name::String
    disabled::Bool
end

LifeEvent(date::Date, name::String) = LifeEvent(date, name, false)

@vars Model begin 
  newLifeEventDate::R{Date} = Dates.today()
  newLifeEventName::R{String} = ""
  newLifeEventBtn::R{Bool} = false
  resetLifeEventBtn::R{Bool} = false

  chosenLifeEvents::R{Vector{LifeEvent}} = []
  toggleLifeEvent::R{Int} = -1 

  uplSearchResults::R{Vector} = []
  scSearchResults::R{Vector} = []
#    Tables.rowtable(results)

  #search_str::R{String} = ""
  #search_btn::R{Bool} = false
  search_loading::R{Bool} = false

  leftDrawerOpen::R{Bool} = false
  rightDrawerOpen::R{Bool} = false
  toggleLeftDrawer::R{Bool} = false
  toggleRightDrawer::R{Bool} = false
end

function search!(model::Model)
#    Tables.rowtable(results)
    n_results = 50
    lifeevents = model.chosenLifeEvents[]
    querymat = [pyconvert.(Vector{Float32}, ft.get_sentence_vector.(x.name for x in lifeevents))...;;]
    queryscores = pairwise(dist, querymat, senmat)

    sc_scores = []
    upl_scores = []
    for li in eachindex(lifeevents)
        lifeevents[li].disabled && continue
        risc = DataFrame(rdf)
        risc.score = queryscores[li, :]
        risc = combine(groupby(risc, :recordPosition), :score => minimum => :score)
        push!(sc_scores, risc)
        riupl = leftjoin(risc, upl2sc[!, [:recordPosition, :UPL_Id]], on=:recordPosition)
        dropmissing!(riupl, :UPL_Id)
        # drop "UPL-nog niet beschikbaar"
        riupl = riupl[riupl.UPL_Id .!= 1, :]
        riupl = combine(groupby(riupl, :UPL_Id), :score => minimum => :score)
        push!(upl_scores, riupl)
    end

#    sc_score = DataFrame(recordPosition=sc_scores[1].recordPosition,
#                         score=reduce(Broadcast.BroadcastFunction(min), [x.score for x in sc_scores]))
#    upl_score = DataFrame(UPL_Id=upl_scores[1].UPL_Id,
#                          score=reduce(Broadcast.BroadcastFunction(min), [x.score for x in upl_scores]))
    sc_score = sc_scores[1][:,[:recordPosition]]
    sc_score.scores = collect.(zip([x.score for x in sc_scores]...))
    sc_score.score = minimum.(sc_score.scores)
    sort!(sc_score, :score)
    sc_score = first(sc_score, n_results)
    leftjoin!(sc_score, sc, on=:recordPosition)

    upl_score = upl_scores[1][:,[:UPL_Id]]
    upl_score.scores = collect.(zip([x.score for x in upl_scores]...))
    upl_score.score = minimum.(upl_score.scores)
    sort!(upl_score, :score)
    upl_score = first(upl_score, n_results)
    leftjoin!(upl_score, upl, on=:UPL_Id)

#    top_sc = sc[first(sort(sc_score, :score).recordPosition, 50), :]
#    top_upl = upl[first(sort(upl_score, :score).UPL_Id, 50), :]
    
#    model.scSearchResults[] = ScSearchResult.(eachrow(top_sc))
#    model.uplSearchResults[] = UplSearchResult.(eachrow(top20upl))

    model.scSearchResults[] = Tables.rowtable(sc_score)
    model.uplSearchResults[] = Tables.rowtable(upl_score)
end

function ui(model::Model)
  onbutton(model.newLifeEventBtn) do (_...)
    push!(model.chosenLifeEvents[], LifeEvent(model.newLifeEventDate[], model.newLifeEventName[]))
    model.newLifeEventName[] = ""
    model.chosenLifeEvents[] = model.chosenLifeEvents[]
  end
  onbutton(model.resetLifeEventBtn) do (_...)
    empty!(model.chosenLifeEvents[])
    model.chosenLifeEvents[] = model.chosenLifeEvents[]
    model.newLifeEventDate[] = today()
  end
  on(model.toggleLifeEvent) do (_...)
    i = model.toggleLifeEvent[]+1
    if i > 0 
      model.chosenLifeEvents[][i].disabled = !model.chosenLifeEvents[][i].disabled
      model.toggleLifeEvent[] = -1
      model.chosenLifeEvents[] = model.chosenLifeEvents[]
    end
  end
  on(model.chosenLifeEvents) do (_...)
    model.search_loading[] = true
    if isempty(x for x in model.chosenLifeEvents[] if !x.disabled)
        model.scSearchResults[] = []
        model.uplSearchResults[] = []
    else
        search!(model)
    end
    model.search_loading[] = false
  end
  onbutton(model.toggleLeftDrawer) do (_...)
    model.leftDrawerOpen[] = !model.leftDrawerOpen[]
  end
  onbutton(model.toggleRightDrawer) do (_...)
    model.rightDrawerOpen[] = !model.rightDrawerOpen[]
  end

  page(
    model, class="container", title="Regelingen voor Levensgebeurtinissen", 
    head_content=
        Genie.Assets.favicon_support()*
        style("""
            .lifeevent {
                cursor: pointer;
            }
            .lifeevent:hover {
                opacity: 60%;
            }
        """), 
      [
      #StippleUI.layout(view="lhh LpR fFf", [
      StippleUI.layout("", view! = "'lHh Lpr fFf'", [
        Stipple.header("", :elevated, class="bg-primary text-white", height__hint="98", [
          StippleUI.toolbar([
            StippleUI.btn("", icon="menu", @click(:toggleLeftDrawer), :dense, :flat, :round)
            StippleUI.toolbartitle([
              StippleUI.avatar([
                img(src="https://cdn.quasar.dev/logo-v2/svg/logo-mono-white.svg")
              ])
            ])
            StippleUI.btn("", icon="menu", @click(:toggleRightDrawer), :dense, :flat, :round)
          ])
          StippleUI.spinner(@iif(:search_loading), color="green",size="3em")
#          StippleUI.tabgroup(align="left", [
#            StippleUI.tab(label="UPL resultaten")
#            StippleUI.tab(label="SC resultaten")
#          ])
        ])
        StippleUI.drawer("", :show__if__above, @bind(:leftDrawerOpen), side="left", :bordered, [
          Stipple.label("Datum levensgebeurtenis")
          StippleUI.datepicker(:newLifeEventDate)
          StippleUI.textfield("Omschrijving levensgebeurtenis", :newLifeEventName, @on("keyup.enter", "newLifeEventBtn = true"))
          StippleUI.btn("Toevoegen", color="primary", @click(:newLifeEventBtn))
          StippleUI.btn("Reset", @click(:resetLifeEventBtn))
          StippleUI.timeline(layout="loose", [
            StippleUI.timelineentry(class="lifeevent", title! = "x.name", subtitle! = "x.date", icon! = "x.disabled ? 'close' : 'star'", color! = "x.disabled ? 'black' : 'blue'",
                @click("toggleLifeEvent = i"), @recur(:"(x, i) in chosenLifeEvents"))
            #StippleUI.timelineentry(title! = "x.name", subtitle! = "x.date", icon! = "x.disabled ? 'close' : 'star'", @click("x.disabled = !x.disabled; notify(chosenLifeEvents); newLifeEventBtn = true"), @recur(:"(x, i) in chosenLifeEvents"))
            #StippleUI.timelineentry(title! = "x.name", @recur(:"x in chosenLifeEvents"))
          ])
        ])
        StippleUI.page_container(class="container", [
          row(@recur(:"x in uplSearchResults"), [
            cell(class="st-module", [
              p([
                b("{{x.UniformeProductnaam}}")
              ])
              Stipple.Html.div(style="float:right;", [
                chip("{{s}}", :dense, :outline, :square, color="black", @recur(:"s in x.scores"))
              ])
              chip("Rijk", :dense, :outline, color="red", @iif("x.Rijk"))
              chip("Province", :dense, :outline, color="pink", @iif("x.Provincie"))
              chip("Waterschap", :dense, :outline, color="purple", @iif("x.Waterschap"))
              chip("Gemeente", :dense, :outline, color="deep-purple", @iif("x.Gemeente"))
              chip("Burger", :dense, :outline, color="indigo", @iif("x.Burger"))
              chip("Bedrijf", :dense, :outline, color="blue", @iif("x.Bedrijf"))
              chip("Dienstenwet", :dense, :outline, color="light-blue", @iif("x.Dienstenwet"))
              chip("Autonomie", :dense, :outline, color="cyan", @iif("x.Autonomie"))
              chip("Medebewind", :dense, :outline, color="teal", @iif("x.Medebewind"))
              chip("Aanvraag", :dense, :outline, color="green", @iif("x.Aanvraag"))
              chip("Melding", :dense, :outline, color="light-green", @iif("x.Melding"))
              chip("Verplichting", :dense, :outline, color="lime", @iif("x.Verplichting"))
              chip("DigiDMacht", :dense, :outline, color="orange", @iif("x.DigiDMacht"))
            ])
          ])
        ])
        StippleUI.drawer("", :show__if__above, @bind(:rightDrawerOpen), side="right", :bordered, width! = "this.\$el.parentElement.scrollWidth * .5", [
          Stipple.Html.div(class="container", [
            row(@recur(:"x in scSearchResults"), [
              cell(class="st-module", [
                heading("{{x.title}}")
                chip("{{x.modified}}", icon="event", @iif("x.modified != ''"))
                chip("{{x.spatialType}}", color="primary", @iif("x.spatialType"))
                chip("{{x.authority__}}", color="teal", @iif("x.authority__"))
                chip("{{x.subject__}}", color="red", @iif("x.subject__"))
                chip("{{x.onlineAanvragen}}", icon="directions", color="deep-orange", @iif("x.onlineAanvragen != ''"))
                p("Uniforme Productnaam: {{x.uniformeProductnaam__}}", @iif("x.uniformeProductnaam__"), style="font-weight:bold;")
                p("{{ x.abstract }}")
              ])
            ])
          ])
        ])
      ])
    ]
  )
end

route("/") do
  Model |> init |> ui |> html
end

up(9001; async = false)
