using XMLDict 
using DataFrames
using OrderedCollections
using Glob
using Arrow

const RAW_SC_DATA_PATH = joinpath("data","raw","sc")
const PROCESSED_DATA_PATH = joinpath("data","processed")

# Recursive function to parse nersted dict to a flattened dict
function flatten_xmldict(dict::OrderedDict)
    return collect(Iterators.flatten(map(flatten_xmldict, collect(dict))))
end
function flatten_xmldict((k1, v1)::Pair{String, OrderedDict{Any, Any}})
    return collect(Iterators.flatten([flatten_xmldict("$(k1)__$k2" => v2) for (k2, v2) in flatten_xmldict(v1)]))
end
#XMLDict parses xml attribute names into a Symbol
function flatten_xmldict((k1, v1)::Pair{Symbol, OrderedDict{Any, Any}})
    return collect(Iterators.flatten([flatten_xmldict("ATTR_$(k1)__$k2" => v2) for (k2, v2) in flatten_xmldict(v1)]))
end
function flatten_xmldict((k1,v1)::Pair{String})
    return [k1 => v1]
end
function flatten_xmldict((k1,v1)::Pair{Symbol})
    return ["attr_$k1" => v1]
end
function flatten_xmldict((k1,v1)::Pair{Any, Any})
    return flatten_xmldict(k1 => v1)
end
# base case
function flatten_xmldict(a::Any)
    return a
end

function parse_record(r)
    f = flatten_xmldict(r)
    return [k=>[v] for (k,v) in f]
end

parsed_records = []
for filename in glob("startRecord*.xml", RAW_SC_DATA_PATH)
    body = read(filename)
    doc_dict = XMLDict.parse_xml(String(body))
    records = doc_dict["records"]["record"]
    append!(parsed_records, DataFrame.(Dict.(parse_record.(xml_dict.(records)))))
end
sc = vcat(parsed_records..., cols=:union)
parsed_records = nothing

# using CSV
# mkpath(INTERM_DATA_PATH)
# CSV.write(joinpath(INTERM_DATA_PATH, "sc.csv"), sc)

useful_columns = [
#extraRecordData__accept, # (n_unique = 1, n_missing = 0)
#extraRecordData__rank, # (n_unique = 1, n_missing = 0)
#extraRecordData__timestamp, # (n_unique = 54, n_missing = 0)
#extraRecordData__version, # (n_unique = 1, n_missing = 0)
#recordData__gzd__attr_schemaLocation, # (n_unique = 1, n_missing = 0)
:recordPosition, # (n_unique = 53914, n_missing = 0)
:recordData__gzd__enrichedData__uniformeProductnaamUri, # (n_unique = 2458, n_missing = 14888)
:recordData__gzd__enrichedData__gerelateerdProductUri, # (n_unique = 350, n_missing = 53331)
:recordData__gzd__enrichedData__authorityScheme, # (n_unique = 6, n_missing = 0)
:recordData__gzd__enrichedData__authorityUri, # (n_unique = 392, n_missing = 0)
:recordData__gzd__enrichedData__spatialType, # (n_unique = 5, n_missing = 0)
:recordData__gzd__enrichedData__spatialUri, # (n_unique = 376, n_missing = 0)
#recordData__gzd__originalData__scproduct__attr_owms-version, # (n_unique = 1, n_missing = 0)
#recordData__gzd__originalData__scproduct__body__productHTML__attr_schemaLocation, # (n_unique = 1, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__, # (n_unique = 388, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__attr_resourceIdentifier, # (n_unique = 392, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__attr_scheme, # (n_unique = 6, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__identifier, # (n_unique = 53914, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__language, # (n_unique = 7, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__modified, # (n_unique = 1705, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__, # (n_unique = 375, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__attr_resourceIdentifier, # (n_unique = 376, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__attr_scheme, # (n_unique = 5, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmskern__title, # (n_unique = 23295, n_missing = 0)
#recordData__gzd__originalData__scproduct__meta__owmskern__type__, # (n_unique = 1, n_missing = 0)
#recordData__gzd__originalData__scproduct__meta__owmskern__type__attr_scheme, # (n_unique = 1, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__abstract, # (n_unique = 43637, n_missing = 10)
#recordData__gzd__originalData__scproduct__meta__owmsmantel__audience, # (n_unique = 3, n_missing = 35271)
:recordData__gzd__originalData__scproduct__meta__scmeta__onlineAanvragen, # (n_unique = 3, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__scmeta__productID, # (n_unique = 28229, n_missing = 0)
#recordPacking, # (n_unique = 1, n_missing = 0)
#recordSchema, # (n_unique = 1, n_missing = 0)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__audience__, # (n_unique = 3, n_missing = 18643)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__audience__attr_scheme, # (n_unique = 2, n_missing = 18643)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject__, # (n_unique = 18, n_missing = 22990)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject__attr_scheme, # (n_unique = 2, n_missing = 22990)
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__, # (n_unique = 1152, n_missing = 16957)
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__attr_resourceIdentifier, # (n_unique = 1390, n_missing = 16957)
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__attr_scheme, # (n_unique = 2, n_missing = 16957)
:recordData__gzd__originalData__scproduct__meta__scmeta__aanvraagURL__attr_resourceIdentifier, # (n_unique = 5991, n_missing = 45695)
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject, # (n_unique = 596, n_missing = 51554)
# TODO evt interresant?
#:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam, # (n_unique = 1069, n_missing = 51845)
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct, # (n_unique = 216, n_missing = 53667)
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__, # (n_unique = 135, n_missing = 53578)
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__attr_resourceIdentifier, # (n_unique = 135, n_missing = 53578)
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__attr_scheme, # (n_unique = 2, n_missing = 53578)
]

sc = sc[:, useful_columns]

DataFrames.rename!(sc, [
:recordData__gzd__enrichedData__uniformeProductnaamUri  => :uniformeProductnaamUri,
:recordData__gzd__enrichedData__gerelateerdProductUri   => :gerelateerdProductUri,
:recordData__gzd__enrichedData__authorityScheme         => :authorityScheme,
:recordData__gzd__enrichedData__authorityUri            => :authorityUri,
:recordData__gzd__enrichedData__spatialType             => :spatialType,
:recordData__gzd__enrichedData__spatialUri              => :spatialUri,
#:recordData__gzd__originalData__scproduct__attr_owms-version => :owms_version,
#:recordData__gzd__originalData__scproduct__body__productHTML__attr_schemaLocation => :attr_schemaLocation,
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__                                => :authority__,
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__attr_resourceIdentifier         => :authority_resourceIdentifier,
:recordData__gzd__originalData__scproduct__meta__owmskern__authority__attr_scheme                     => :authority_scheme,
:recordData__gzd__originalData__scproduct__meta__owmskern__identifier                                 => :identifier,
:recordData__gzd__originalData__scproduct__meta__owmskern__language                                   => :language,
:recordData__gzd__originalData__scproduct__meta__owmskern__modified                                   => :modified,
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__                                  => :spatial__,
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__attr_resourceIdentifier           => :spatial_resourceIdentifier,
:recordData__gzd__originalData__scproduct__meta__owmskern__spatial__attr_scheme                       => :spatial_scheme,
:recordData__gzd__originalData__scproduct__meta__owmskern__title                                      => :title,
#recordData__gzd__originalData__scproduct__meta__owmskern__type__ => :type__,
#recordData__gzd__originalData__scproduct__meta__owmskern__type__attr_scheme => :type_scheme,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__abstract                                 => :abstract,
#recordData__gzd__originalData__scproduct__meta__owmsmantel__audience => :audience,
:recordData__gzd__originalData__scproduct__meta__scmeta__onlineAanvragen                              => :onlineAanvragen,
:recordData__gzd__originalData__scproduct__meta__scmeta__productID                                    => :productID,
#recordPacking => #recordPacking,
#recordSchema => #recordSchema,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__audience__                               => :audience__,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__audience__attr_scheme                    => :audience_scheme,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject__                                => :subject__,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject__attr_scheme                     => :subject_scheme,
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__                        => :uniformeProductnaam__,
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__attr_resourceIdentifier => :uniformeProductnaam_resourceIdentifier,
:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam__attr_scheme             => :uniformeProductnaam_scheme,
:recordData__gzd__originalData__scproduct__meta__scmeta__aanvraagURL__attr_resourceIdentifier         => :aanvraagURL_resourceIdentifier,
:recordData__gzd__originalData__scproduct__meta__owmsmantel__subject                                  => :subject,
#:recordData__gzd__originalData__scproduct__meta__scmeta__uniformeProductnaam => #:uniformeProductnaam,
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct                           => :gerelateerdProduct,
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__                         => :gerelateerdProduct__,
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__attr_resourceIdentifier  => :gerelateerdProduct_resourceIdentifier,
:recordData__gzd__originalData__scproduct__meta__scmeta__gerelateerdProduct__attr_scheme              => :gerelateerdProduct_scheme,
])

# :uniformeProductnaamUri is genoeg en kan ook gebruikt wroden wanneer een regeling is gekoppeld aan eerdere UPLs
select!(sc, Not([:uniformeProductnaam__, 
                #:uniformeProductnaam, 
                :uniformeProductnaam_resourceIdentifier, 
                #:uniformeProductnaam_scheme,
#drop columns which can not be saved as arrow
               :subject,
               :gerelateerdProduct,
        ]))

ensure_vector(x::Vector) = x
ensure_vector(::Missing) = []
ensure_vector(x) = [x]

sc.UPL_URI = ensure_vector.(sc.uniformeProductnaamUri)
sc.n_UPL = length.(sc.UPL_URI)

sc.recordPosition = parse.(Int, sc.recordPosition)
sort!(sc, :recordPosition)

sc[ismissing.(sc.abstract),:abstract] .= ""
disallowmissing!(sc, :abstract)

mkpath(PROCESSED_DATA_PATH)
Arrow.write(joinpath(PROCESSED_DATA_PATH, "sc.arrow"), sc)
