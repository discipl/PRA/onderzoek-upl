using HTTP
using DataFrames
using Arrow
using CSV

const PROCESSED_DATA_PATH = joinpath("data", "processed")

############################################################################ sdg
sdg_resp = HTTP.get("https://standaarden.overheid.nl/owms/oquery/UPL-SDG-Informatiegebied.csv")

# TODO? save source csv?
#sdg_csv_str = String(copy(sdg_rep.body))

sdg = CSV.read(sdg_resp.body, DataFrame)

#sdg[!,[:SDG_Code, :SDG_Informatiegebied, :SDG_Thema, :SDG_URI]]
sdg = sdg[!, startswith.(names(sdg), "SDG_")]
unique!(sdg)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "sdg.arrow"), sdg)
############################################################################ /sdg

############################################################################ upl
upl_resp = HTTP.get("https://standaarden.overheid.nl/owms/oquery/UPL-actueel.csv")

# TODO? save source csv?
#upl_csv_str = String(copy(upl_rep.body))

upl = CSV.read(upl_resp.body, DataFrame)

rename!(upl, :URI => :UPL_URI)

upl.UPL_Slug = lowercase.(last.(split.(upl.UPL_URI, '/')))

     ###################################################### grondslagen
grondslagen = upl[:, [:UPL_Slug, :Grondslag, :Grondslaglabel, :Grondslaglink]]

# drop columns having other values with the dublicates
# TODO: Make mapping grondslagen table for these values
select!(upl, Not([:Grondslag, :Grondslaglabel, :Grondslaglink]))

unique!(upl)

upl.UPL_Id = rownumber.(eachrow(upl))


             ################################ upl2sdg
upl.SDG = coalesce.(upl.SDG, "")
upl.SDG = convert.(String, upl.SDG)
upl.SDG_Code = split.(upl.SDG, ';', keepempty=false)
upl.SDG_Code = convert.(Vector{String}, upl.SDG_Code)

upl2sdg = flatten(select(upl, :UPL_Id, :SDG_Code), :SDG_Code)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "upl2sdg.arrow"), upl2sdg)
             ################################ /upl2sdg

# select colum nnames of the columns which do not contain other unique values than missing and "X"
boolean_columns = issubset.(Set.(unique.(eachcol(upl))), Ref(Set([missing, "X"])))
@show names(upl, boolean_columns)

upl[!, boolean_columns] .= .~ismissing.(upl[!, boolean_columns])

Arrow.write(joinpath(PROCESSED_DATA_PATH, "upl.arrow"), upl)
############################################################################ /upl

leftjoin!(grondslagen, upl[!,[:UPL_Slug, :UPL_Id]], on=:UPL_Slug)
disallowmissing!(grondslagen, :UPL_Id)

grondslagen = grondslagen[.~all.(eachrow(ismissing.(grondslagen[!, [:Grondslag, :Grondslaglabel, :Grondslaglink]]))), :]

disallowmissing!(grondslagen, error=false)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "grondslagen.arrow"), grondslagen)
     ###################################################### /grondslagen

