using DataFrames
using Arrow

const PROCESSED_DATA_PATH = joinpath("data", "processed")

const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))
const upl = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl.arrow")))

# many 2 many relation
upl2sc = flatten(select(sc, :recordPosition, :UPL_URI, :n_UPL), :UPL_URI)
upl2sc.UPL_Slug = lowercase.(last.(split.(upl2sc.UPL_URI, '/')))

leftjoin!(upl2sc, select(upl, :UPL_Slug, :UPL_Id), on=:UPL_Slug)

sort!(upl2sc, :recordPosition)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "upl2sc.arrow"), upl2sc)

