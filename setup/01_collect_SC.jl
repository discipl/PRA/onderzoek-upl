using HTTP
using EzXML
using Printf

const RAW_SC_DATA_PATH = joinpath("data","raw","sc")
const MAXIMUM_RECORDS = 1000
mkpath(RAW_DATA_PATH)

start_record = 1
while true
    print("startRecord: ", start_record)
    resp = HTTP.get("https://zoekdienst.overheid.nl/sru/Search?version=1.2&operation=searchRetrieve&x-info-1-accept=any&x-connection=sc&startRecord=$start_record&maximumRecords=$MAXIMUM_RECORDS&query=keyword=\"\"")
    doc = parsexml(resp.body)
    write(joinpath(RAW_SC_DATA_PATH, @sprintf("startRecord-%06i.xml", start_record)), doc)
    r = root(doc)
    println("\tnumberOfRecords: ", nodecontent(EzXML.nodes(r)[2]))
    nodename(EzXML.nodes(r)[4]) == "nextRecordPosition" || break
    start_record = parse(Int, nodecontent(EzXML.nodes(r)[4]))
    sleep(5)
end
