using DataFrames
using Arrow
using PythonCall

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const EXTERNAL_DATA_PATH = joinpath("data", "external")

fasttext = pyimport("fasttext")
ft = fasttext.load_model(joinpath(EXTERNAL_DATA_PATH, "cc.nl.64.bin"))

const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))

sc_sentences = sc[:,[:recordPosition]]

sc_sentences.Sentence_Text = split.(replace.(sc.abstract, ". " => "\n"), "\n")

sc_sentences = flatten(sc_sentences, :Sentence_Text)

sc_sentences.Sentence_Text = strip.(sc_sentences.Sentence_Text)

sc_sentences = sc_sentences[.~isempty.(sc_sentences.Sentence_Text), :]

sc_sentences.Sentence_Vec = ft.get_sentence_vector.(sc_sentences.Sentence_Text)

sc_sentences.Sentence_Vec = pyconvert.(Vector{Float32}, sc_sentences.Sentence_Vec)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "sc_sentences.arrow"), sc_sentences)
